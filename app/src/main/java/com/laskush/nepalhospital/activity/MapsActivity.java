package com.laskush.nepalhospital.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.laskush.nepalhospital.MapDirection.DirectionFinder;
import com.laskush.nepalhospital.MapDirection.DirectionFinderListener;
import com.laskush.nepalhospital.MapDirection.Route;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.utility.NetworkManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,DirectionFinderListener {

    private GoogleMap mMap;
    private LocationManager locationManager;

    LatLng sourcelatlng;
    LatLng destLatLng= new LatLng(27.7048249,85.3136514);
    LatLng hospitalLatLng;

    String origin;
    String destination;
    String distance;
    String duration;
    String end_address;

    private ProgressDialog progressDialog;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();

    Marker currentMarker;
    Marker destinationMarker;
    Context context;
    Intent i;
    String destinationTitle,destinationAddress;
    double destinationlat,destinationLon;

    //Views
    CardView detailLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        context = getApplicationContext();

        i = getIntent();
        destinationTitle = i.getStringExtra("name");
        destinationAddress = i.getStringExtra("address");
        destinationlat = Double.parseDouble(i.getStringExtra("latitude"));
        destinationLon = Double.parseDouble(i.getStringExtra("longitude"));

        //Toast.makeText(MapsActivity.this, "Latitude : "+ destinationlat, Toast.LENGTH_SHORT).show();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(destinationTitle);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
        } else {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);

            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            float zoomLevel = 18.0f; //This goes up to 21

            LatLng latLng = new LatLng(destinationlat,destinationLon);

            int height = 90;
            int width = 70;
            //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.custom_marker);
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.custom_location);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap currentlocationMarker = Bitmap.createScaledBitmap(b, width, height, false);

            currentMarker = currentMarker = mMap.addMarker(new MarkerOptions()
                    .position(getCurrentLocation())
                    .title("You are here")
                    .icon(BitmapDescriptorFactory.fromBitmap(currentlocationMarker))
                    .snippet("Current Location"));

            destinationMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(destinationTitle)
                    .snippet(destinationAddress));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            /*currentMarker = mMap.addMarker(new MarkerOptions()
                    //.position(new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude))
                    //.position(new LatLng(27.6704621,85.2500948))
                    .position(new LatLng(destinationlat,destinationLon))
                    .title("You destination")
                    .icon(BitmapDescriptorFactory.fromBitmap(currentlocationMarker))
                    .snippet(destinationTitle));*/



            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.addCircle(new CircleOptions()
                    .center(new LatLng(destinationlat,destinationLon))
                    .strokeWidth(3.0f)
            );


            boolean result = true;
            blink(result);

            if (latLng == null) {


            } else {

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationlat,destinationLon), zoomLevel));
                setUpMap();
            }


            /*LatLng latLng = getCurrentLocation();

            int height = 90;
            int width = 70;
            //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.custom_marker);
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.custom_location);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap currentlocationMarker = Bitmap.createScaledBitmap(b, width, height, false);

           *//* LatLng current = new LatLng(27.7048249,85.3136514);
            currentMarker = mMap.addMarker(new MarkerOptions().position(current).title("You are here")
                    .snippet("Current Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(current));*//*

            currentMarker = mMap.addMarker(new MarkerOptions()
                    //.position(new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude))
                    //.position(new LatLng(27.6704621,85.2500948))
                    .position(new LatLng(destinationlat,destinationLon))
                    .title("You are here")
                    .icon(BitmapDescriptorFactory.fromBitmap(currentlocationMarker))
                    .snippet("Current Location"));

            *//*currentMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(27.4740326,85.7769164))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.custom_location))
                    .title("you am here")
                    .snippet("Current Location")
            );*//*


            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.addCircle(new CircleOptions()
                    //.center(new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude))
                   // .center(new LatLng(27.6704621,85.2500948))
                    .center(new LatLng(destinationlat,destinationLon))
                    .strokeWidth(3.0f)
            );


            boolean result = true;
            blink(result);

      *//*      // Add a marker in destination and move the camera
            LatLng birhospital = new LatLng(27.7048249,85.3136514);
            mMap.addMarker(new MarkerOptions().position(birhospital).title(destinationTitle).snippet(destinationAddress));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(birhospital));*//*

            if (latLng == null) {


            } else {

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationlat,destinationLon), zoomLevel));
                setUpMap();
            }*/
        }

    }

    private LatLng getCurrentLocation() {
        LatLng currentLatLant = null;
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        locationManager = (LocationManager) this
                .getSystemService(LOCATION_SERVICE);
        if (locationManager == null) {

        } else {


            Location bestLocation = null;
            for (String provider : providers) {

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return null;
                }
                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
//

            try{
                currentLatLant = new LatLng(bestLocation.getLatitude(), bestLocation.getLongitude());
            }catch (NullPointerException e){
                e.printStackTrace();
                currentLatLant = new LatLng(27.6704621,85.2500948);
            }

        }


        return currentLatLant;
        //return  new LatLng(27.6704621,85.2500948);


    }

    private void setUpMap() {

        detailLayout=(CardView) findViewById(R.id.detail_layout);
        detailLayout.setVisibility(View.GONE);


                sourcelatlng = getCurrentLocation();
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {


                        destLatLng = marker.getPosition();

                        double sourceLat = sourcelatlng.latitude;
                        double destLat = destLatLng.latitude;
                        double sourceLog = sourcelatlng.longitude;
                        double destLog = destLatLng.longitude;

                        origin = Double.toString(sourceLat) + "," + Double.toString(sourceLog);
                        destination =  Double.toString(destLat) + "," + Double.toString(destLog);


                        NetworkManager manager = new NetworkManager();
                        boolean GPS_status =  CheckGpsStatus();
                        boolean internetPresence = manager.isInternetConnectionAvailable(MapsActivity.this);

                        if(GPS_status == true && internetPresence == true){

                            //call();

                        }else{
                            if(GPS_status == false && internetPresence == false){

                                Toast.makeText(MapsActivity.this, "Location and Network not enabled", Toast.LENGTH_SHORT).show();

                            }else if(GPS_status == true && internetPresence == false){

                                Toast.makeText(MapsActivity.this, "Network Not Available", Toast.LENGTH_SHORT).show();

                            }else if(GPS_status == false && internetPresence == true){

                                Toast.makeText(MapsActivity.this, "Location not enabled", Toast.LENGTH_SHORT).show();
                            }

                        }

                        if(marker.getTitle().equals("I am here")){}
                        else {
                            setMarkerDetails(marker.getTitle(),marker.getSnippet());
                        }

                        return true;
                    }
                });



                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        detailLayout.setVisibility(View.INVISIBLE);
                    }
                });
    }

    public void setMarkerDetails(String title, final String address){

        TextView titleTv, addressTv,distance,duration;
        detailLayout.setVisibility(View.VISIBLE);
        distance= (TextView)findViewById(R.id.distance);
        distance.setVisibility(View.GONE);
        duration= (TextView)findViewById(R.id.duration);
        duration.setVisibility(View.GONE);

        titleTv= (TextView)findViewById(R.id.detail_title_text_view);
        titleTv.setText(title);

        addressTv= (TextView)findViewById(R.id.detail_address_text_view);
        addressTv.setText(address);

//        distanceTv = (TextView)findViewById(R.id.distance);
//        durationTv = (TextView)findViewById(R.id.duration);

//        distanceTv.setText("Distance: " + distance);
//        durationTv.setText("Duration: " + duration);

    }


    public void call(){

        try {
            new DirectionFinder((DirectionFinderListener)this, origin,destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public boolean CheckGpsStatus() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean GpsStatus = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        return GpsStatus;

    }

    private void blink(boolean result) {

        final boolean resu = result;
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 1000;    //in milissegunds
                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    boolean check = resu;
                    @Override
                    public void run() {
                        boolean res = false;
//                        TextView txt = (TextView) findViewById(R.id.usage);
                        if (check == true) {
                            currentMarker.setVisible(false);
                            res = false;
                        } else if(check == false){
                            currentMarker.setVisible(true);
                            res = true;
                        }
                        blink(res);
                    }
                });
            }
        }).start();
    }


    @Override
    public void onDirectionFinderStart() {


        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }

    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {

        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();


        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 13));

            end_address = route.endAddress.toString();
            distance = route.distance.text;
            duration = route.duration.text;

            TextView distanceTv,durationTv;
            detailLayout.setVisibility(View.VISIBLE);

            distanceTv = (TextView)findViewById(R.id.distance);
            durationTv = (TextView)findViewById(R.id.duration);

            distanceTv.setText("Distance: " + distance);
            durationTv.setText("Duration: " + duration);

            distanceTv.setVisibility(View.GONE);
            durationTv.setVisibility(View.GONE);

            int height = 90;
            int width = 70;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.custom_marker);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            if(sourcelatlng.equals(destLatLng)){

                Toast.makeText(this, "Source and Destination is same", Toast.LENGTH_SHORT).show();
            }else{

               /* originMarkers.add(mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.custom_location))
                        .title(route.startAddress)
                        .position(sourcelatlng)));
                destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                        .title(route.endAddress)
                        .position(destLatLng)));*/

            }


            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(8);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));

            List<LatLng> points = route.points; // route is instance of PolylineOptions

            LatLngBounds.Builder bc = new LatLngBounds.Builder();

            for (LatLng item : points) {
                bc.include(item);
            }

            LatLngBounds bounds = bc.build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 60);
            mMap.moveCamera(cameraUpdate);

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
