package com.laskush.nepalhospital.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.laskush.nepalhospital.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AmbulanceActivity extends AppCompatActivity {

    //Views
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //variables
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        toolbar.setTitle(getString(R.string.hospital));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(AmbulanceActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) +
                ContextCompat.checkSelfPermission(AmbulanceActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) +
                ContextCompat.checkSelfPermission(AmbulanceActivity.this,
                        Manifest.permission.ACCESS_NETWORK_STATE) +
                ContextCompat.checkSelfPermission(AmbulanceActivity.this,
                        Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if ( ActivityCompat.shouldShowRequestPermissionRationale
                    (AmbulanceActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (AmbulanceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (AmbulanceActivity.this, Manifest.permission.ACCESS_NETWORK_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {

                Snackbar.make(AmbulanceActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .ACCESS_FINE_LOCATION,Manifest.permission
                                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                                .ACCESS_NETWORK_STATE,Manifest.permission
                                                .CALL_PHONE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .ACCESS_FINE_LOCATION,Manifest.permission
                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                .ACCESS_NETWORK_STATE,Manifest.permission
                                .CALL_PHONE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }
}
