package com.laskush.nepalhospital.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.fragment.DashBoardFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //constants
    private final String TAG = MainActivity.class.getSimpleName();

    //views
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;


    //variables
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;
    boolean isDashBoardSelected, doubleBackToExitPressedOnce = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(MainActivity.this);
        navigationView.getMenu().getItem(0).setChecked(true);
        Menu menu = navigationView.getMenu();

        menu.getItem(0).setChecked(true);
        setDashBoardSelected(true);
        setupDisplay();
    }


    public void setDashBoardSelected(boolean isSelected) {
        this.isDashBoardSelected = isSelected;
    }

    public boolean isDashBoardSelected() {
        return isDashBoardSelected;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            DashBoardFragment fragment = new DashBoardFragment();
            FragmentManager frgManager = getSupportFragmentManager();
            FragmentTransaction trans = frgManager.beginTransaction();
            trans.replace(R.id.frame, fragment);
            trans.commit();
        }else if(id == R.id.nav_hospital){
            navigationView.getMenu().getItem(1).setChecked(false);
            Intent hospitalIntent=new Intent(MainActivity.this, HospitalActivity.class);
            startActivity(hospitalIntent);
        }else if(id == R.id.nav_ambulance){
            Intent ambulanceIntent=new Intent(MainActivity.this, AmbulanceActivity.class);
            startActivity(ambulanceIntent);
        }else if(id == R.id.nav_health_organization){
            Intent homeopathicIntent=new Intent(MainActivity.this, HomeopathicActivity.class);
            startActivity(homeopathicIntent);
        }else if(id == R.id.nav_oxygen){
            Intent oxygenIntent=new Intent(MainActivity.this,OxygenActivity.class);
            startActivity(oxygenIntent);
        }else if(id == R.id.nav_clinic){
            Intent therapyIntent=new Intent(MainActivity.this, TherapyActivity.class);
            startActivity(therapyIntent);
        }else if(id == R.id.nav_ayurvedic){
            Intent ayurvedaIntent=new Intent(MainActivity.this, AyurvedaActivity.class);
            startActivity(ayurvedaIntent);
        }else if(id == R.id.nav_peoples_health){
            Intent healthintent=new Intent(MainActivity.this, HealthCampaignActivity.class);
            startActivity(healthintent);
        }else if(id == R.id.nav_nathuropathy){
            Intent nathuropathyIntent=new Intent(MainActivity.this, NathuropathyActivity.class);
            startActivity(nathuropathyIntent);
        }else if(id == R.id.nav_about){
            Intent aboutIntent=new Intent(MainActivity.this, AboutActivity.class);
            startActivity(aboutIntent);
        }else if(id == R.id.nav_feedback){
            Intent feedbackIntent=new Intent(MainActivity.this, FeedbackActivity.class);
            startActivity(feedbackIntent);
        }


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupDisplay() {
        DashBoardFragment fragment = new DashBoardFragment();
        FragmentManager frgManager = getSupportFragmentManager();
        FragmentTransaction trans = frgManager.beginTransaction();
        trans.replace(R.id.frame, fragment);
        trans.commit();
    }


    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) +
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) +
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_NETWORK_STATE) +
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if ( ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.ACCESS_NETWORK_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.CALL_PHONE)) {

                Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .ACCESS_FINE_LOCATION,Manifest.permission
                                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                                .ACCESS_NETWORK_STATE,Manifest.permission
                                                .CALL_PHONE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .ACCESS_FINE_LOCATION,Manifest.permission
                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                .ACCESS_NETWORK_STATE,Manifest.permission
                                .CALL_PHONE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }

}
