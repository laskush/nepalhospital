package com.laskush.nepalhospital.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.laskush.nepalhospital.Model.Hospital.HospitalDTO;
import com.laskush.nepalhospital.Model.Hospital.HospitalResponse;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.Retrofit.APIServices;
import com.laskush.nepalhospital.Retrofit.RetrofitApiClient;
import com.laskush.nepalhospital.adapter.HomeopathicAdapter;
import com.laskush.nepalhospital.utility.AppUtil;
import com.laskush.nepalhospital.utility.NetworkManager;
import com.laskush.nepalhospital.utility.PrefUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeopathicActivity extends AppCompatActivity {

    //Views
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_search)
    ImageButton btnSearch;

    @BindView(R.id.hosp_list)
    RecyclerView hospList;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.no_search_data)
    TextView noSearchData;

    @BindView(R.id.search_progress)
    ProgressBar progressBar;

    @BindView(R.id.search_bar)
    SearchView search;

    @BindView(R.id.error_textView)
    TextView errorTextView;


    //variables
    private String error;
    //private HospitalAdapter hospitalAdapter;
    private ArrayList<HospitalDTO> homeopathicList = new ArrayList();
    private JSONArray jsonArray;
    private JSONObject jsonObject;
    private LinearLayoutManager linearLayoutManager;
    private boolean loading = true;
    private int offset = 0;
    private int offsetLoaded = 0;
    private int pastVisibleItems;
    private String response;
    private boolean searching = false;
    private int totalItemCount;
    private String url;
    private int visibleItemCount;
    ProgressDialog progressDialog;

    //instances
     HomeopathicAdapter homeopathicAdapter;

    //variables
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);
        ButterKnife.bind(this);

        toolbar.setTitle(getString(R.string.homeopathic));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        noSearchData.setHint(R.string.hint_search_homeopathic);

        if(NetworkManager.isInternetConnectionAvailable(HomeopathicActivity.this)){
            showProgressDialog(true);
            getHomeopathicList();
        }else{
            showSnackbar(getResources().getString(R.string.no_internet));
            homeopathicList = PrefUtils.getHomeopathicList(HomeopathicActivity.this,PrefUtils.HOMEOPATHIC_RESPONSE);
            if(homeopathicList!= null){
                setHomeopathicList(homeopathicList);
            }
        }


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                homeopathicAdapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private void getHomeopathicList() {

        final Retrofit retrofit = new RetrofitApiClient(HomeopathicActivity.this).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getHomeopathic().enqueue(new Callback<HospitalResponse>() {

            @Override
            public void onResponse(Call<HospitalResponse> call, Response<HospitalResponse> response) {
                try {

                    if (response.body() != null) {
                        HospitalResponse playListResponse = response.body();
                        Log.d("Release Response : ", String.valueOf(response.body()));
                        homeopathicList = playListResponse.getHospitalArrayList();
                        PrefUtils.saveHomeopathicList(HomeopathicActivity.this,homeopathicList,PrefUtils.HOMEOPATHIC_RESPONSE);
                        showProgressDialog(false);

                        if(homeopathicList!=null){
                            setHomeopathicList(homeopathicList);
                        }


                    } else {
                        errorTextView.setVisibility(View.VISIBLE);
                        errorTextView.setText(getResources().getString(R.string.no_data));
                        showProgressDialog(false);
                        showSnackbar(getResources().getString(R.string.no_data));
                    }


                } catch (NullPointerException e) {
                    //showProgressDialog(false);
                    showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<HospitalResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                errorTextView.setVisibility(View.VISIBLE);
                errorTextView.setText(getResources().getString(R.string.no_data));
                showSnackbar(getResources().getString(R.string.no_data));
                showProgressDialog(false);
            }
        });

        /*for (int i = 0; i < 15; i++) {
            homeopathicList.add(new HospitalDTO(1, "Hariharbhawan, Lalitpur, Nepal", "01-55220927,5552381", "", "Shree Pashupati Homeopathic Hospital",""));
        }*/

    }


    private void showProgressDialog(boolean shouldShow) {
        if(shouldShow){
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(HomeopathicActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
            }
        }else{
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog=null;
            }
        }

    }

    void setHomeopathicList(ArrayList<HospitalDTO> hospitalArrayList){

        Collections.sort(hospitalArrayList, HospitalDTO.CommNameComparator);

        homeopathicAdapter = new HomeopathicAdapter(getLayoutInflater(), HomeopathicActivity.this);
        homeopathicAdapter.addHomeopathic(hospitalArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeopathicActivity.this);

        hospList.setLayoutManager(layoutManager);
        hospList.setItemAnimator(new DefaultItemAnimator());
        hospList.setAdapter(homeopathicAdapter);

    }

    void showSnackbar(String message){
        Snackbar.make(toolbar, message, Snackbar.LENGTH_LONG).show();

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){

            if(NetworkManager.CheckGpsStatus(HomeopathicActivity.this) == true){
                showSnackbar("Location  enabled");
                int position = homeopathicAdapter.clickedItemPosition ;
                AppUtil.CallingMapActivtyFromHospital(HomeopathicActivity.this,homeopathicList.get(position).getName(),
                        homeopathicList.get(position).getAddress(),homeopathicList.get(position).getLatitude(),homeopathicList.get(position).getLongitude());
            }else{
                showSnackbar("Location not enabled");
            }

        }

        if(requestCode == 2){

            if(NetworkManager.isInternetConnectionAvailable(HomeopathicActivity.this) == true){
                showSnackbar("Network  enabled");
                int position = homeopathicAdapter.clickedItemPosition ;
                AppUtil.CallingMapActivtyFromHospital(HomeopathicActivity.this,homeopathicList.get(position).getName(),
                        homeopathicList.get(position).getAddress(),
                        homeopathicList.get(position).getLatitude(),homeopathicList.get(position).getLongitude());
            }else{
                showSnackbar("Network not enabled");
            }

        }


        if(requestCode == 3){

            if(NetworkManager.isInternetConnectionAvailable(HomeopathicActivity.this) == true){
                showSnackbar("Network  enabled");
                /*int position = hospitalAdapter.clickedItemPosition ;
                AppUtil.CallingMapActivtyFromHospital(HospitalActivity.this,hospitalList.get(position).getName(),
                        hospitalList.get(position).getAddress(),
                        hospitalList.get(position).getLatitude(),hospitalList.get(position).getLongitude());*/

                //dialog for enabling internet

                final Dialog dialog = new Dialog(HomeopathicActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_gpslocation);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();


                Button Yesbtn = (Button)dialog.findViewById(R.id.gpslocation_Yes);
                Button NoButton=(Button)dialog.findViewById(R.id.gpslocation_No);

                Yesbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        //startActivityForResult(intent1,1);
                        startActivityForResult(intent1,1);

                    }
                });

                NoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        //System.exit(1);
                    }
                });


            }else{
                showSnackbar("Network not enabled");
            }

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(HomeopathicActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) +
                ContextCompat.checkSelfPermission(HomeopathicActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) +
                ContextCompat.checkSelfPermission(HomeopathicActivity.this,
                        Manifest.permission.ACCESS_NETWORK_STATE) +
                ContextCompat.checkSelfPermission(HomeopathicActivity.this,
                        Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if ( ActivityCompat.shouldShowRequestPermissionRationale
                    (HomeopathicActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (HomeopathicActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (HomeopathicActivity.this, Manifest.permission.ACCESS_NETWORK_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (HomeopathicActivity.this, Manifest.permission.CALL_PHONE)) {

                Snackbar.make(HomeopathicActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .ACCESS_FINE_LOCATION,Manifest.permission
                                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                                .ACCESS_NETWORK_STATE,Manifest.permission
                                                .CALL_PHONE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .ACCESS_FINE_LOCATION,Manifest.permission
                                .ACCESS_COARSE_LOCATION ,Manifest.permission
                                .ACCESS_NETWORK_STATE,Manifest.permission
                                .CALL_PHONE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }


}
