package com.laskush.nepalhospital.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.utility.NetworkManager;
import com.laskush.nepalhospital.utility.PrefUtils;

public class SplashActivity extends AppCompatActivity {

    private static String TAG = SplashActivity.class.getSimpleName();
    private static int SPLASH_TIME =5000;

    RelativeLayout top_image_rl;
    Animation anim;
    int backpress = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        top_image_rl = (RelativeLayout) findViewById(R.id.top_image_rl);
        anim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        top_image_rl.startAnimation(anim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(PrefUtils.isFirstRun(SplashActivity.this).equalsIgnoreCase("true")){

                    if(NetworkManager.isInternetConnectionAvailable(SplashActivity.this)){
                        PrefUtils.saveFirstRun(SplashActivity.this,"false");
                        Intent i  =new Intent(SplashActivity.this,MainActivity.class);
                        finish();
                        startActivity(i);
                    }else{
                        showInternetDialog();
                    }

                }else{
                    PrefUtils.saveFirstRun(SplashActivity.this,"false");
                    Intent i  =new Intent(SplashActivity.this,MainActivity.class);
                    finish();
                    startActivity(i);
                }


            }
        },SPLASH_TIME);


       /* ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        imageView.startAnimation(pulse);*/
    }

    private void showInternetDialog() {

        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_network);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        Button Okbtn = (Button)dialog.findViewById(R.id.network_OK);
        Okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent2 = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivityForResult(intent2,2);
                //System.exit(1);
            }
        });
    }


    void showSnackbar(String message) {
        Snackbar.make(top_image_rl, message,
                Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == 2){

            if(NetworkManager.isInternetConnectionAvailable(SplashActivity.this) == true){
                showSnackbar("Internet connected");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PrefUtils.saveFirstRun(SplashActivity.this,"false");
                        Intent i  =new Intent(SplashActivity.this,MainActivity.class);
                        finish();
                        startActivity(i);
                    }
                },1000);

            }else{
                showSnackbar("Internet is not connected");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        System.exit(1);
                    }
                },1000);

            }

        }

    }

    public void onBackPressed(){
        backpress = (backpress + 1);
        Toast.makeText(getApplicationContext(), " Press Back again to Exit ", Toast.LENGTH_SHORT).show();

        if (backpress>1) {
            this.finish();
        }
    }
}
