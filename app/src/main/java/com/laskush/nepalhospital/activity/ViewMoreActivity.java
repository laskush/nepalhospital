package com.laskush.nepalhospital.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.utility.NetworkManager;
import com.laskush.nepalhospital.utility.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewMoreActivity extends AppCompatActivity{

    //Views
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_photoView)
    PhotoView view_photoView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    //variables
    Intent i;
    String title,ad_image;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewmore);

        ButterKnife.bind(this);

        i = getIntent();
        title = i.getStringExtra("title");
        ad_image = i.getStringExtra("ad_image");
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        progressBar.setVisibility(View.VISIBLE);

        //Glide.with(ViewMoreActivity.this).load(ad_image).error(this.getResources().getDrawable(R.drawable.logo)).into(view_photoView);


        if(NetworkManager.isInternetConnectionAvailable(ViewMoreActivity.this) == true){

            Glide.with(ViewMoreActivity.this).load(ad_image).error(this.getResources().getDrawable(R.drawable.error_image)).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
                    .placeholder(this.getResources().getDrawable(R.drawable.error_image))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(view_photoView);
        }else{
            Glide.with(ViewMoreActivity.this).load(ad_image).error(this.getResources().getDrawable(R.drawable.error_image)).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
                    .placeholder(this.getResources().getDrawable(R.drawable.error_image))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(view_photoView);

            showSnackbar(getString(R.string.no_internet));
        }


    }


    void showSnackbar(String message){
        Snackbar.make(toolbar, message, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
