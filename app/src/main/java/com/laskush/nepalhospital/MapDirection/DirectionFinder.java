package com.laskush.nepalhospital.MapDirection;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajan Shrestha on 2/13/2017.
 */
public class DirectionFinder {


    String  response = "{\n" +
            "   \"geocoded_waypoints\" : [\n" +
            "      {\n" +
            "         \"geocoder_status\" : \"OK\",\n" +
            "         \"place_id\" : \"ChIJ5Y-1qMci6zkRJ603t9YgEQg\",\n" +
            "         \"types\" : [\n" +
            "            \"bus_station\",\n" +
            "            \"establishment\",\n" +
            "            \"point_of_interest\",\n" +
            "            \"transit_station\"\n" +
            "         ]\n" +
            "      },\n" +
            "      {\n" +
            "         \"geocoder_status\" : \"OK\",\n" +
            "         \"partial_match\" : true,\n" +
            "         \"place_id\" : \"ChIJCyA3_P8Y6zkRvAaR6dNxxPw\",\n" +
            "         \"types\" : [ \"establishment\", \"health\", \"hospital\", \"point_of_interest\" ]\n" +
            "      }\n" +
            "   ],\n" +
            "   \"routes\" : [\n" +
            "      {\n" +
            "         \"bounds\" : {\n" +
            "            \"northeast\" : {\n" +
            "               \"lat\" : 27.7047707,\n" +
            "               \"lng\" : 85.3141442\n" +
            "            },\n" +
            "            \"southwest\" : {\n" +
            "               \"lat\" : 27.670458,\n" +
            "               \"lng\" : 85.25010449999999\n" +
            "            }\n" +
            "         },\n" +
            "         \"copyrights\" : \"Map data ©2018 Google\",\n" +
            "         \"legs\" : [\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"9.1 km\",\n" +
            "                  \"value\" : 9062\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"34 mins\",\n" +
            "                  \"value\" : 2014\n" +
            "               },\n" +
            "               \"end_address\" : \"Kanti Path, Kathmandu 44600, Nepal\",\n" +
            "               \"end_location\" : {\n" +
            "                  \"lat\" : 27.7047707,\n" +
            "                  \"lng\" : 85.3139274\n" +
            "               },\n" +
            "               \"start_address\" : \"Machchhegaun, Chandragiri 44618, Nepal\",\n" +
            "               \"start_location\" : {\n" +
            "                  \"lat\" : 27.670458,\n" +
            "                  \"lng\" : 85.25010449999999\n" +
            "               },\n" +
            "               \"steps\" : [\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"1.9 km\",\n" +
            "                        \"value\" : 1877\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"5 mins\",\n" +
            "                        \"value\" : 310\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6813685,\n" +
            "                        \"lng\" : 85.2618215\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Head \\u003cb\\u003enorth\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"kk{gDcligOKG[GWCU@KC[Sk@c@c@Yc@][WSS[Q[M_@Qc@W]OYOe@[i@a@g@m@SUSMQG[c@Sc@KKWIY?[Qo@S_Ae@yB{Ao@]OCOLQ^OJOBmBoAi@K[CMIQUOOi@Q]QMKKQMGOCOIQOCMAOB_@ASGMSOGKEUAa@GWI_@GmAI}@Ac@Dc@Ls@@WEs@Cu@EOQQi@e@]c@[UQOIOM]Sa@MYOc@Oo@Ms@M_@SU[QCAWGKKESCa@Ea@O_@Ok@OY[_@YSKOQa@UUSE]D\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.670458,\n" +
            "                        \"lng\" : 85.25010449999999\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.4 km\",\n" +
            "                        \"value\" : 373\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 77\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6831135,\n" +
            "                        \"lng\" : 85.26473469999999\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue straight\",\n" +
            "                     \"maneuver\" : \"straight\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"qo}gDkukgO[DUBYGOMOUYg@Wa@eAyBWaAKo@M]IQE]Mg@YuAMa@\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6813685,\n" +
            "                        \"lng\" : 85.2618215\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.8 km\",\n" +
            "                        \"value\" : 823\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"3 mins\",\n" +
            "                        \"value\" : 157\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6879891,\n" +
            "                        \"lng\" : 85.26986959999999\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Slight \\u003cb\\u003eleft\\u003c/b\\u003e\",\n" +
            "                     \"maneuver\" : \"turn-slight-left\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"mz}gDqglgOKQ]e@QMWWY]QUGQKa@KSu@_B]eA]sAMw@]}@U}@]iAUg@KQKIMM_@YKMIWCWOMGCKEICWGWAOEOMSMUI_@Ga@MEAc@SQEO?QLOJK?[Mc@YICKCO@]N\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6831135,\n" +
            "                        \"lng\" : 85.26473469999999\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"1.3 km\",\n" +
            "                        \"value\" : 1326\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"8 mins\",\n" +
            "                        \"value\" : 470\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6933961,\n" +
            "                        \"lng\" : 85.28160439999999\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e onto \\u003cb\\u003eKalankisthan Rd\\u003c/b\\u003e/\\u003cb\\u003ePrithvi Hwy\\u003c/b\\u003e/\\u003cb\\u003eTribhuvan Highway\\u003c/b\\u003e\",\n" +
            "                     \"maneuver\" : \"turn-right\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"}x~gDugmgOaBkB]]}@eA{@eASUa@o@OWO]iDkKi@eBeAgDWg@Y_@cA{Aa@}@Oa@IWI]CYC[Gs@Ou@AYAGIo@GYEOwBuGQcASoAy@}F\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6879891,\n" +
            "                        \"lng\" : 85.26986959999999\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"1.3 km\",\n" +
            "                        \"value\" : 1285\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"5 mins\",\n" +
            "                        \"value\" : 271\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6966472,\n" +
            "                        \"lng\" : 85.2936442\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue onto \\u003cb\\u003eGaneshman Singh Path\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"wz_hD_qogOAYA[AYC[CYEm@Ky@E_@Kk@Ge@Ia@G[IWKe@Qo@o@{CCKEUIc@Os@COa@eCMk@c@yB[gBKe@Ik@k@mDGg@Ag@AKA[AIAMAOCOC]Ic@CQEOESEUEe@?Y`@eD@GAgAIy@Ii@CEEIEKKMKKMMMIIKOIOKKEUM[QOMOQKS\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6933961,\n" +
            "                        \"lng\" : 85.28160439999999\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.8 km\",\n" +
            "                        \"value\" : 838\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"4 mins\",\n" +
            "                        \"value\" : 225\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6981824,\n" +
            "                        \"lng\" : 85.3017868\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue onto \\u003cb\\u003eGaneshman Singh Rd\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"ao`hDg|qgOMc@k@iCqAyG[mA_AeEIq@GWK]k@iCg@}CE_@CUAa@Cm@AK@]@WFm@RuANyAFq@@Y\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6966472,\n" +
            "                        \"lng\" : 85.2936442\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.4 km\",\n" +
            "                        \"value\" : 416\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"2 mins\",\n" +
            "                        \"value\" : 103\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6966942,\n" +
            "                        \"lng\" : 85.30561730000001\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue onto \\u003cb\\u003eKalimati Rd\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"sx`hDeosgODi@PuA@EJ_ADSLw@He@Jc@J_@Rg@j@qATi@BIb@w@nA{C\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6981824,\n" +
            "                        \"lng\" : 85.3017868\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.9 km\",\n" +
            "                        \"value\" : 892\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"3 mins\",\n" +
            "                        \"value\" : 154\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.6938268,\n" +
            "                        \"lng\" : 85.3140289\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue onto \\u003cb\\u003eTripura Marg\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"io`hDcgtgOx@yDXuAF]He@TkAXaAJa@JWnAiE~@oCnB{Gv@}CPw@J]BKRkALi@F_@No@Ey@\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6966942,\n" +
            "                        \"lng\" : 85.30561730000001\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"1.2 km\",\n" +
            "                        \"value\" : 1232\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"4 mins\",\n" +
            "                        \"value\" : 247\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 27.7047707,\n" +
            "                        \"lng\" : 85.3139274\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"At the roundabout, take the \\u003cb\\u003e1st\\u003c/b\\u003e exit onto \\u003cb\\u003eKanti Path\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003eDestination will be on the left\\u003c/div\\u003e\",\n" +
            "                     \"maneuver\" : \"roundabout-left\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"m}_hDu{ugOAA?AA??AAAoAMS?GA}ADs@Dm@^]BqFZYBm@BQBu@PuAZ]Fa@BsAAG?o@AiAAa@Ca@AW?M?E?UAWAM?e@CI?sBG_DGA?q@Ai@CaDM_AEQ?{@@S?E?_@AYCOCKAm@Ok@KEA\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 27.6938268,\n" +
            "                        \"lng\" : 85.3140289\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  }\n" +
            "               ],\n" +
            "               \"traffic_speed_entry\" : [],\n" +
            "               \"via_waypoint\" : []\n" +
            "            }\n" +
            "         ],\n" +
            "         \"overview_polyline\" : {\n" +
            "            \"points\" : \"kk{gDcligOg@OWCU@KC[SoA}@_Au@SS[Q{@_@aAg@_Ak@i@a@g@m@g@c@QG[c@_@o@WIY?kAe@_Ae@yB{Ao@]OCOLQ^OJOBmBoAi@K[CMIa@e@gAc@Y]]Ka@YE]B_@ASGMSOMa@Iy@I_@GmAKaBRwACkACu@EO{@w@]c@[U[_@a@_A]}@]cBM_@SU_@SWGKKIu@Ea@O_@_@eAu@s@]q@UUSEy@JUBYGOMOUq@iAeAyBWaAKo@M]Oo@g@}BYs@]e@QMq@u@Yg@Wu@u@_B]eA]sAMw@s@{Bs@qBW[m@g@KMIWCWOMSIa@Kg@Gc@[u@Qg@Ou@YO?QLOJK?_Ag@UGO@]N_CiCyBkCu@eA_@u@sEqNeAgDWg@}A{Bq@_BSu@Gu@Gs@Ou@Ca@QiA}BeHe@sCy@}FAYCu@C[IgAQyAe@oCU}@Qo@o@{CIa@YwAe@uCq@eDg@mCu@yEIoACg@Iw@WcBKi@E_Ab@mDAgAIy@Mo@KUWY[WYU[Qq@_@_@_@Yw@}BcL{AsGQiAw@gDm@}DKqBBu@ZcCVkCFcAd@oDV}AVcAxAmDb@w@nA{CrAoGPcAn@mCfDsKnB{Gv@}C\\\\uAl@aDNo@Ey@AAACAAcBMeBBs@Dm@^oG^gAFgATsBb@a@BsAAw@AeDG_CGgIQ}GWuB?i@Gy@Qq@M\"\n" +
            "         },\n" +
            "         \"summary\" : \"Tribhuvan Highway\",\n" +
            "         \"warnings\" : [],\n" +
            "         \"waypoint_order\" : []\n" +
            "      }\n" +
            "   ],\n" +
            "   \"status\" : \"OK\"\n" +
            "}\n";
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    // private static final String GOOGLE_API_KEY = "AIzaSyDfxoZfLC4ZjqDJ9t67Blvnm5PmVqSHfFo";
    // private static final String GOOGLE_API_KEY = "AIzaSyA43gqU4RS0dkSeD_GwtoeQWOHE11B60b0";
    //key for all
     //private static final String GOOGLE_API_KEY = "AIzaSyCAcfy-02UHSu2F6WeQ1rhQhkCr51eBL9g";
    //key for hospital
     //private static final String GOOGLE_API_KEY = "AIzaSyBj-cnmMUY21M0vnIKz0k3tD3bRdyZea-Y";
     private static final String GOOGLE_API_KEY = "AIzaSyAVaM1fdoBTa6eGut1SG5smOlwMtQ5-sAE";
     private DirectionFinderListener listener;
     private String origin;
    private String destination;
    private Context context;

    public DirectionFinder(Context context) {

        this.context = context;
        this.listener = (DirectionFinderListener) this.context;

    }


    public DirectionFinder(DirectionFinderListener listner, String origin, String destination) {

        this.listener = listner;
        this.origin = origin;
        this.destination = destination;
    }




    public void execute() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createUrl());
    }

    private String createUrl() throws UnsupportedEncodingException {
        String urlOrigin = URLEncoder.encode(origin, "utf-8");
        String urlDestination = URLEncoder.encode(destination, "utf-8");

        return DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&key=" + GOOGLE_API_KEY;
      //  return DIRECTION_URL_API + "origin=" + "Disneyland" + "&destination=" + "Universal+Studios+Hollywood4" + "&key=" + GOOGLE_API_KEY;
       // return DIRECTION_URL_API + "origin=" + "27.4740326,85.7769164" + "&destination=" + "27.7048249,85.3136514" + "&key=" + GOOGLE_API_KEY;
    }




    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                //Toast.makeText(context,"RESULT : "+res,Toast.LENGTH_SHORT).show();
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    private void parseJSon(String data) throws JSONException {
        //data = response;
        if (data == null)
            return;

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.points = decodePolyLine(overview_polylineJson.getString("points"));

            routes.add(route);
        }

        listener.onDirectionFinderSuccess(routes);
    }



    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }

}
