package com.laskush.nepalhospital.MapDirection;

import java.util.List;

/**
 * Created by Rajan Shrestha on 2/13/2017.
 */
public interface DirectionFinderListener {

    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
