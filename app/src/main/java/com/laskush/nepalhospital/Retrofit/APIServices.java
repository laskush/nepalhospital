package com.laskush.nepalhospital.Retrofit;

import com.laskush.nepalhospital.Model.Hospital.HospitalResponse;
import com.laskush.nepalhospital.Model.Slider.SliderResponse;
import com.laskush.nepalhospital.utility.AppText;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;


public interface APIServices {

    @Headers({
            "Content-Type: application/json"
    })


    @GET(AppText.SLIDER_URL)
    Call<SliderResponse> getSlider(
    );

    @GET(AppText.HOSPITAL_URL)
    Call<HospitalResponse> getHospital(
    );

    @GET(AppText.HOMEOPATHIC_URL)
    Call<HospitalResponse> getHomeopathic(
    );

    @GET(AppText.OXYGEN_URL)
    Call<HospitalResponse> getOxygen(
    );

    @GET(AppText.THERAPY_URL)
    Call<HospitalResponse> getTherapy(
    );

    @GET(AppText.AYURVEDA_URL)
    Call<HospitalResponse> getAyurveda(
    );

    @GET(AppText.NATHUROPATHY)
    Call<HospitalResponse> getNathuropathy(
    );

    @GET(AppText.PUBLIC_HEALTH_C_URL)
    Call<HospitalResponse> getPublicHealthCampaign(
    );


}
