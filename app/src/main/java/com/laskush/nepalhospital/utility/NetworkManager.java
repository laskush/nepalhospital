package com.laskush.nepalhospital.utility;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class NetworkManager {
    public static boolean isInternetConnectionAvailable(Context context){

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI")){
                    if (ni.isConnected()) haveConnectedWifi = true;
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE")){
                    if (ni.isConnected()) haveConnectedMobile = true;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return haveConnectedWifi || haveConnectedMobile;

    }

    public static boolean CheckGpsStatus(Context context) {

        LocationManager locationManager;
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        boolean GpsStatus = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        return GpsStatus;
    }

}
