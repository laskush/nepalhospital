package com.laskush.nepalhospital.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laskush.nepalhospital.Model.Hospital.HospitalDTO;
import com.laskush.nepalhospital.Model.Slider.SliderDTO;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PrefUtils {
    public static final String SHARED_PREF_SONG_BANG = "nepal_hospital";
    public static final String FIRST_TIME_RUN = "first_time_run";

    public static final String AYURVEDA_RESPONSE = "ayurveda_response";
    public static final String SLIDER_RESPONSE = "slider_response";
    public static final String HEALTH_C_RESPONSE = "healrh_c_response";
    public static final String HOMEOPATHIC_RESPONSE = "homeopathic_response";
    public static final String HOSPITAL_RESPONSE = "hospital_response";
    public static final String NATHUROPATHY_RESPONSE = "nathuropathy_response";
    public static final String OXYGEN_RESPONSE = "oxygen_response";
    public static final String THERAPY_RESPONSE = "therapy_response";


    public static void saveFirstRun(Context context, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FIRST_TIME_RUN, value);
        editor.commit();
    }

    public static String isFirstRun(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(FIRST_TIME_RUN, "true");

    }


    public static void saveSliderList(Context context, ArrayList<SliderDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<SliderDTO> getSliderList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<SliderDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveAyurvedaList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getAyurvedaList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveHealthCampaignList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getHealthCampaignList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveHomeopathicList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getHomeopathicList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveHospitalList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getHospitalList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveNathuropathyList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getNathuropathyList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveOxygenList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getOxygenList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveTherapyList(Context context, ArrayList<HospitalDTO> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<HospitalDTO> getTherapyList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HospitalDTO>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

}
