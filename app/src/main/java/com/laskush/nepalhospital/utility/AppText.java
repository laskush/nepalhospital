package com.laskush.nepalhospital.utility;

public class AppText {

    public  static final String BASE_URL="http://laskush.com/medical/";

    public static final String SLIDER_URL = "api/slideshow";
    public static final String HOSPITAL_URL = "api/hospital";
    public static final String HOMEOPATHIC_URL ="api/homeopathic";
    public static final String OXYGEN_URL ="api/oxygen";
    public static final String THERAPY_URL ="api/therapy";
    public static final String AYURVEDA_URL ="api/ayurveda";
    public static final String PUBLIC_HEALTH_C_URL ="api/phc";
    public static final String NATHUROPATHY ="api/nathuropathy";
    public static final String FEEDBACK_URL ="api/sendMail/";



}
