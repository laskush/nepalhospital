package com.laskush.nepalhospital.utility;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.laskush.nepalhospital.activity.MapsActivity;

import java.util.ArrayList;


/**
 * Created by nitsdl on 12/23/14.
 */
public class AppUtil {


    /**find if there is a working internet connection available in device*/
    public static boolean isInternetConnectionAvailable(Context context){

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI")){
                    if (ni.isConnected()) haveConnectedWifi = true;
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE")){
                    if (ni.isConnected()) haveConnectedMobile = true;
                }

            }
        } catch (Exception e) {
            e.getStackTrace();
        }

        return haveConnectedWifi || haveConnectedMobile;

    }





    public static String getFontStyle(String htmlBodyContents){
        return "<html>" +
                "<head>" +
                "<style type=\"text/css\">" +
                "@font-face{ " +
                "font-family: \"Roboto\";" +
                "src: url('file:///android_asset/fonts/Roboto-Light.ttf');}" +
                "body {" +
                "font-family: \"Roboto\";" +
                "line-height: \"normal\";}" +
                "</style>" +
                "</head>" +
                "<body>" +
                htmlBodyContents +
                "</body>" +
                "</html>";
    }



    public static ArrayList<String> getcontact(String phone, String substr) {

        int length = phone.length();
        int pre_index = 0;

        ArrayList<String> phoneNumber = new ArrayList<>();
        int index = phone.indexOf(substr);

        while (index >= 0) {
            phoneNumber.add(phone.substring(pre_index, index));
            pre_index = index + 1;
            if(phone.substring(pre_index,length).contains(substr)){
                index = phone.indexOf(substr, pre_index);
            }else{
                phoneNumber.add(phone.substring(pre_index, length));
                index = -1;
            }
        }

        return phoneNumber;
    }


    public static void CallingMapActivty(Context mContext,String name, String address){
        Intent mapsActivityIntent = new Intent(mContext,MapsActivity.class);
        mapsActivityIntent.putExtra("name",name);
        mapsActivityIntent.putExtra("address",address);
        mContext.startActivity(mapsActivityIntent);

    }

    public static void CallingMapActivtyFromHospital(Context mContext,String name, String address,String latitude,String longitude){
        Intent mapsActivityIntent = new Intent(mContext,MapsActivity.class);
        mapsActivityIntent.putExtra("name",name);
        mapsActivityIntent.putExtra("address",address);
        mapsActivityIntent.putExtra("latitude",latitude);
        mapsActivityIntent.putExtra("longitude",longitude);
        mContext.startActivity(mapsActivityIntent);

    }

}
