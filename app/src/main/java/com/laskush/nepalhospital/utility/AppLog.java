package com.laskush.nepalhospital.utility;


import android.util.Log;

import com.laskush.nepalhospital.BuildConfig;


/**
 * Created by lokex on 4/16/15.
 */
public class AppLog {

    public static void showLog(String tag, String message){
        if(BuildConfig.DEBUG) Log.d(tag, message);
    }
}
