package com.laskush.nepalhospital.Model.Hospital;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HospitalResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<HospitalDTO> hospitalArrayList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<HospitalDTO> getHospitalArrayList() {
        return hospitalArrayList;
    }

    public void setHospitalArrayList(ArrayList<HospitalDTO> hospitalArrayList) {
        this.hospitalArrayList = hospitalArrayList;
    }
}
