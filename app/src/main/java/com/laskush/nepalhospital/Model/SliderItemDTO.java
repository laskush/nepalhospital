package com.laskush.nepalhospital.Model;

public class SliderItemDTO {
    String id;
    private String name;
    private int icon;
    String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public SliderItemDTO(String name, int icon) {
        this.name = name;
        this.icon = icon;

    }
    public SliderItemDTO() {


    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }


}
