package com.laskush.nepalhospital.Model.Slider;

import com.google.gson.annotations.SerializedName;

public class SliderDTO {

    @SerializedName("title")
    String title;

    @SerializedName("image")
    String image;

    @SerializedName("caption")
    String caption;

    @SerializedName("location")
    String location;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
