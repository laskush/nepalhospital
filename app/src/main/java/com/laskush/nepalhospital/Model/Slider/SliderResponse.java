package com.laskush.nepalhospital.Model.Slider;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SliderResponse {

    @SerializedName("status")
    String status;

    @SerializedName("records")
    ArrayList<SliderDTO> sliderDTOList = new ArrayList<>();


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SliderDTO> getSliderDTOList() {
        return sliderDTOList;
    }

    public void setSliderDTOList(ArrayList<SliderDTO> sliderDTOList) {
        this.sliderDTOList = sliderDTOList;
    }
}
