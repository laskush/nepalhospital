package com.laskush.nepalhospital.Model;

public class DoctorDTO {

    private int doctor_id;
    private String doctor_image;
    private String doctor_name;
    private String hospital_address;
    private int hospital_id;
    private String hospital_image;
    private String hospital_name;
    private String specialization;

    public DoctorDTO(int doctor_id, String doctor_image, String doctor_name, String hospital_address,
                     int hospital_id, String hospital_image, String hospital_name, String specialization) {
        this.doctor_id = doctor_id;
        this.doctor_image = doctor_image;
        this.doctor_name = doctor_name;
        this.hospital_address = hospital_address;
        this.hospital_id = hospital_id;
        this.hospital_image = hospital_image;
        this.hospital_name = hospital_name;
        this.specialization = specialization;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getDoctor_image() {
        return doctor_image;
    }

    public void setDoctor_image(String doctor_image) {
        this.doctor_image = doctor_image;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getHospital_address() {
        return hospital_address;
    }

    public void setHospital_address(String hospital_address) {
        this.hospital_address = hospital_address;
    }

    public int getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(int hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getHospital_image() {
        return hospital_image;
    }

    public void setHospital_image(String hospital_image) {
        this.hospital_image = hospital_image;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
