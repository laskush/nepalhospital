package com.laskush.nepalhospital.Model.Hospital;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class HospitalDTO {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("contact")
    private String contact;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("detail_image")
    private String image;
    @SerializedName("detail_image_2")
    private String ad_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAd_image() {
        return ad_image;
    }

    public void setAd_image(String ad_image) {
        this.ad_image = ad_image;
    }

    /*Comparator for sorting the list by hospital Name*/
    public static Comparator<HospitalDTO> CommNameComparator = new Comparator<HospitalDTO>() {

        public int compare(HospitalDTO s1, HospitalDTO s2) {
            String CommunityName1 = s1.getName().toUpperCase();
            String CommunityName2 = s2.getName().toUpperCase();

            //ascending order
            return CommunityName1.compareTo(CommunityName2);

            //descending order
            //return CommunityName2.compareTo(CommunityName1);
        }};
}
