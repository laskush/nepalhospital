package com.laskush.nepalhospital.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laskush.nepalhospital.Interface.RecylerItemClickListner;
import com.laskush.nepalhospital.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mcontactList = new ArrayList<>();
    String TYPE;

    private RecylerItemClickListner clickListener;

    public ContactAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    public ContactAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_contact, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindContact(mcontactList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return mcontactList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addContact(List<String> contactList,String type) {
        mcontactList.addAll(contactList);
        TYPE = type;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for ImportantNumber


        TextView seq_no, contact_tv;
        LinearLayout contact_item;


        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

            seq_no = (TextView) itemView.findViewById(R.id.seq_no);
            contact_tv = (TextView) itemView.findViewById(R.id.contact);
            contact_item= (LinearLayout) itemView.findViewById(R.id.contact_item);

        }

        public void bindContact(final String contact,int position) {

            seq_no.setText(position + 1 +".");
            contact_tv.setText(contact);

            contact_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MakeACall(contact);

                }
            });

        }

        private void MakeACall(String contact) {

            if(TYPE.equalsIgnoreCase("contact") || TYPE.equalsIgnoreCase("mobile")){

                String no = null;
                try {
                    no = URLEncoder.encode(contact, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Intent callIntent = new Intent("android.intent.action.DIAL");
                callIntent.setData(Uri.parse("tel:" + no));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mContext.startActivity(callIntent);

            }else if(TYPE.equalsIgnoreCase("email")){

                Intent intent = new Intent( Intent.ACTION_SEND);
                intent.setType("plain/text");

                intent.putExtra( Intent.EXTRA_EMAIL, new String[] { contact });
                //intent.putExtra(Intent.EXTRA_EMAIL, "rajanshresthance@gmail.com");
                //intent.putExtra( android.content.Intent.EXTRA_SUBJECT, "Subject");
                //intent.putExtra( android.content.Intent.EXTRA_TEXT, "I'm email body.");

                mContext.startActivity(Intent.createChooser(intent, "Send Email"));

            }

        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }
}
