package com.laskush.nepalhospital.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.laskush.nepalhospital.Interface.RecylerItemClickListner;
import com.laskush.nepalhospital.Model.Hospital.HospitalDTO;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.activity.HospitalActivity;
import com.laskush.nepalhospital.activity.ViewMoreActivity;
import com.laskush.nepalhospital.utility.AppUtil;
import com.laskush.nepalhospital.utility.NetworkManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.Holder> implements Filterable {

    Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<HospitalDTO> mhospitalList = new ArrayList<>();
    private ArrayList<HospitalDTO> filterList = new ArrayList<>();

    private RecylerItemClickListner clickListener;
    int lastPosition = -1;
    ArrayList<String> contactList = new ArrayList<>();
    CustomFilter filter;
    public static int clickedItemPosition = 0;

    public HospitalAdapter(LayoutInflater inflater, Context mContext) {
        mLayoutInflater = inflater;
        this.mContext = mContext;

    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_hospital, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindHospital(mhospitalList.get(position),position);
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return mhospitalList.size();
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addHospital(List<HospitalDTO> hospitalDTOList) {
        mhospitalList.addAll(hospitalDTOList);
        filterList.addAll(hospitalDTOList);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.hospital_rl)
        RelativeLayout hospital_rl;

        @BindView(R.id.hospital_imageView)
        ImageView hospital_imageView;

        @BindView(R.id.hospital_image_progressbar)
        ProgressBar hospital_image_progressbar;

        @BindView(R.id.hospital_nametv)
        TextView hospital_nametv;

        @BindView(R.id.hospital_addresstv)
        TextView hospital_addresstv;

        @BindView(R.id.contact_ll)
        LinearLayout contact_ll;

        @BindView(R.id.map_ll)
        public LinearLayout map_ll;


        @BindView(R.id.hospital_contacttv)
        TextView hospital_contacttv;

        @BindView(R.id.hospital_readmoretv)
        TextView hospital_readmoretv;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindHospital(final HospitalDTO hospitalDTO, final int position) {

            hospital_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


            hospital_nametv.setText(hospitalDTO.getName());
            hospital_addresstv.setText(hospitalDTO.getAddress());
            hospital_contacttv.setText(hospitalDTO.getContact());
           // Glide.with(mContext).load(hospitalDTO.getImage()).error(mContext.getResources().getDrawable(R.drawable.slider_error_image)).into(hospital_imageView);


            if (NetworkManager.isInternetConnectionAvailable(mContext) == true) {

                Glide.with(mContext).load(hospitalDTO.getImage()).error(mContext.getResources().getDrawable(R.drawable.slider_error_image)).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        hospital_image_progressbar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                        hospital_image_progressbar.setVisibility(View.GONE);
                        return false;
                    }
                })
                        .placeholder(mContext.getResources().getDrawable(R.drawable.slider_error_image))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(hospital_imageView);
            } else {

                Glide.with(mContext).load(hospitalDTO.getImage()).error(mContext.getResources().getDrawable(R.drawable.slider_error_image)).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        hospital_image_progressbar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                        hospital_image_progressbar.setVisibility(View.GONE);
                        return false;
                    }
                })
                        .placeholder(mContext.getResources().getDrawable(R.drawable.slider_error_image))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(hospital_imageView);

                showSnackbar(mContext.getResources().getString(R.string.no_internet));
            }


            hospital_imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent viewMoreIntent = new Intent(mContext, ViewMoreActivity.class);
                    viewMoreIntent.putExtra("title", hospitalDTO.getName());
                    viewMoreIntent.putExtra("ad_image", hospitalDTO.getImage());
                    mContext.startActivity(viewMoreIntent);
                }
            });

                if (hospitalDTO.getAd_image().equalsIgnoreCase("")) {
                    hospital_readmoretv.setVisibility(View.GONE);
                } else {
                    hospital_readmoretv.setVisibility(View.VISIBLE);
                }

                hospital_readmoretv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent viewMoreIntent = new Intent(mContext, ViewMoreActivity.class);
                        viewMoreIntent.putExtra("title", hospitalDTO.getName());
                        viewMoreIntent.putExtra("ad_image", hospitalDTO.getAd_image());
                        mContext.startActivity(viewMoreIntent);
                    }
                });

                contact_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (hospitalDTO.getContact().length() == 0) {
                            showSnackbar(mContext.getResources().getString(R.string.no_contact));
                            //Toast.makeText(EmployeeDetailActivity.this,getResources().getString(R.string.no_email),Toast.LENGTH_SHORT).show();
                        } else if (hospitalDTO.getContact().contains(",")) {
                            contactList = new ArrayList<>();
                            contactList = AppUtil.getcontact(hospitalDTO.getContact(), ",");
                            showContactDialog(contactList, mContext.getResources().getString(R.string.contact_dialog_title), "contact");


                        } else {
                            contactList = new ArrayList<>();
                            contactList.add(hospitalDTO.getContact().substring(0, hospitalDTO.getContact().length()));
                            showContactDialog(contactList, mContext.getResources().getString(R.string.contact_dialog_title), "contact");
                        }

                    }

                });

                map_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        boolean GPS_status = NetworkManager.CheckGpsStatus(mContext);
                        boolean internetPresence = NetworkManager.isInternetConnectionAvailable(mContext);


                        if (internetPresence == true && GPS_status == true) {


                            final double latitude = Double.parseDouble(hospitalDTO.getLatitude());
                            final double longitude = Double.parseDouble(hospitalDTO.getLongitude());

                            //String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                        /*String uri = "http://maps.google.com/maps?saddr=" + 27.6746363 + "," + 85.3131375 + "&daddr=" + latitude + "," + longitude;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        mContext.startActivity(intent);*/

                            AppUtil.CallingMapActivtyFromHospital(mContext, hospitalDTO.getName(), hospitalDTO.getAddress(), hospitalDTO.getLatitude(), hospitalDTO.getLongitude());
                        } else if ((internetPresence == true && GPS_status == false) || (internetPresence == false && GPS_status == true)) {

                            if (internetPresence == true && GPS_status == false) {

                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.dialog_gpslocation);
                                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                dialog.show();


                                Button Yesbtn = (Button) dialog.findViewById(R.id.gpslocation_Yes);
                                Button NoButton = (Button) dialog.findViewById(R.id.gpslocation_No);

                                Yesbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        clickedItemPosition = position;
                                        dialog.dismiss();
                                        Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        //startActivityForResult(intent1,1);
                                        ((HospitalActivity) mContext).startActivityForResult(intent1, 1);

                                    }
                                });

                                NoButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        //System.exit(1);
                                    }
                                });


                            }

                            if (internetPresence == false && GPS_status == true) {

                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.dialog_network);
                                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                dialog.show();


                                Button Okbtn = (Button) dialog.findViewById(R.id.network_OK);
                                Okbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        clickedItemPosition = position;
                                        dialog.dismiss();
                                        Intent intent2 = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                        ((HospitalActivity) mContext).startActivityForResult(intent2, 2);
                                        //System.exit(1);
                                    }
                                });

                            }
                        } else if (internetPresence == false && GPS_status == false) {

                            clickedItemPosition = position;
                            final Dialog dialog = new Dialog(mContext);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_gps_network);
                            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                            dialog.show();


                            Button Okbtn = (Button) dialog.findViewById(R.id.gps_network_OK);
                            Okbtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Intent intent2 = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    ((HospitalActivity) mContext).startActivityForResult(intent2, 3);
                                    //

                                }
                            });

                        }

                        //AppUtil.CallingMapActivty(mContext,hospitalDTO.getName(),hospitalDTO.getAddress());
                    }
                });

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

        void showSnackbar(String message) {
            Snackbar.make(contact_ll, message,
                    Snackbar.LENGTH_LONG).show();

        }

    }

    @Override
    public Filter getFilter() {
        //return null;
        if(filter==null)
        {
            filter=new CustomFilter(filterList,this);
        }
        return filter;

    }


    private class CustomFilter extends Filter {


        HospitalAdapter adapter;
        ArrayList<HospitalDTO> filterList;

        public CustomFilter(ArrayList<HospitalDTO> filterList, HospitalAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results=new FilterResults();
            if(constraint != null && constraint.length() > 0) {

                //CHANGE TO UPPER
                constraint=constraint.toString().toUpperCase();

                ArrayList<HospitalDTO> filteredList=new ArrayList<>();
                for (int i=0;i<filterList.size();i++)
                {
                    //CHECK
                    if(filterList.get(i).getName().toUpperCase().contains(constraint)
                            || filterList.get(i).getAddress().toUpperCase().contains(constraint)
                            )
                    {
                        //ADD PLAYER TO FILTERED PLAYERS
                        filteredList.add(filterList.get(i));
                    }
                }
                results.count=filteredList.size();
                results.values=filteredList;
            }else {
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {

            adapter.mhospitalList= (ArrayList<HospitalDTO>) results.values;
            //REFRESH
            adapter.notifyDataSetChanged();
        }
    }

    private void showContactDialog(ArrayList<String> emailList,String dialog_title,String type) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.contact_list_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        TextView title = (TextView) dialog.findViewById(R.id.dialogList_title);
        title.setText(dialog_title);
        RecyclerView number_recyclerView = (RecyclerView) dialog.findViewById(R.id.contact_recyclerView);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);

        ContactAdapter contactAdapter;
        contactAdapter = new ContactAdapter(dialog.getLayoutInflater(),mContext);
        contactAdapter.addContact(emailList,type);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);

        number_recyclerView.setLayoutManager(layoutManager);
        number_recyclerView.setItemAnimator(new DefaultItemAnimator());
        number_recyclerView.setAdapter(contactAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}
