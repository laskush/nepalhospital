package com.laskush.nepalhospital.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.laskush.nepalhospital.Model.Slider.SliderDTO;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.activity.ViewMoreActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BannerFragment extends Fragment {

    //constants
    private final String TAG = BannerFragment.class.getSimpleName();

    //variables
    private int position;
    private SliderDTO sliderItemDTO;


    //views
    @BindView(R.id.bannerImageView)
    ImageView imageView;

    @BindView(R.id.progressBarBannerImage)
    ProgressBar progressBarBanner;

    @BindView(R.id.tv_banner_text)
    TextView textViewBnner;

    @BindView(R.id.frame_layout)
    FrameLayout frame_layout;

    public BannerFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public BannerFragment(int position, SliderDTO sliderItem) {
        this.position = position;
        this.sliderItemDTO = sliderItem;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_banner, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarBanner.setVisibility(View.GONE);


        if (sliderItemDTO != null) {

            textViewBnner.setText(Html.fromHtml(sliderItemDTO.getTitle()));

            /*Glide.with(getActivity())
                    .load(sliderItemDTO.getLocation())
                    .error(getContext().getResources().getDrawable(R.drawable.slider_error_image))
                    .into(imageView);*/

            Glide.with(getActivity()).load(sliderItemDTO.getLocation()).error(getContext().getResources().getDrawable(R.drawable.slider_error_image)).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBarBanner.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBarBanner.setVisibility(View.GONE);
                    return false;
                }
            })
                   // .placeholder(getContext().getResources().getDrawable(R.drawable.slider_error_image))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageView);

        } else {

        }

        frame_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewMoreIntent = new Intent(getContext(),ViewMoreActivity.class);
                viewMoreIntent.putExtra("title",sliderItemDTO.getTitle());
                viewMoreIntent.putExtra("ad_image",sliderItemDTO.getLocation());
                startActivity(viewMoreIntent);
            }
        });
    }
}
