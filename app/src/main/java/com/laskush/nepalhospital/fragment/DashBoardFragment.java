package com.laskush.nepalhospital.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.laskush.nepalhospital.Model.Slider.SliderDTO;
import com.laskush.nepalhospital.Model.Slider.SliderResponse;
import com.laskush.nepalhospital.Model.SliderItemDTO;
import com.laskush.nepalhospital.R;
import com.laskush.nepalhospital.Retrofit.APIServices;
import com.laskush.nepalhospital.Retrofit.RetrofitApiClient;
import com.laskush.nepalhospital.activity.AmbulanceActivity;
import com.laskush.nepalhospital.activity.AyurvedaActivity;
import com.laskush.nepalhospital.activity.HealthCampaignActivity;
import com.laskush.nepalhospital.activity.HomeopathicActivity;
import com.laskush.nepalhospital.activity.HospitalActivity;
import com.laskush.nepalhospital.activity.NathuropathyActivity;
import com.laskush.nepalhospital.activity.OxygenActivity;
import com.laskush.nepalhospital.activity.TherapyActivity;
import com.laskush.nepalhospital.utility.NetworkManager;
import com.laskush.nepalhospital.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * A simple {@link Fragment} subclass.

 */
public class DashBoardFragment extends Fragment implements View.OnClickListener {

    //constant
    private static String TAG=DashBoardFragment.class.getSimpleName();

    //Views
    @BindView(R.id.llHospital)
    LinearLayout llHospital;

    @BindView(R.id.llAmbulance)
    LinearLayout llAmbulance;

    @BindView(R.id.llHealth_Organization_Homeopathic)
    LinearLayout llHealth_Organization_Homeopathic;

    @BindView(R.id.llOxygen)
    LinearLayout llOxygen;

    @BindView(R.id.llTherapy_Clinic)
    LinearLayout llTherapy_Clinic;

    @BindView(R.id.llAyurveda_Home)
    LinearLayout llAyurveda_Home;

    @BindView(R.id.llPeoples_Health_Campaign)
    LinearLayout llPeoples_Health_Campaign;

    @BindView(R.id.llNathuropathy_Hospital)
    LinearLayout llNathuropathy_Hospital;

    @BindView(R.id.viewPagerImages)
    ViewPager viewPager;


    //variables
    int currentSliderPage = 0;
    int totalSliderCount;
    List<SliderItemDTO> sliderList;
    ArrayList<SliderDTO> sliderDTOList = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final long ANIM_VIEWPAGER_DELAY = 5000;
    private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;
    private Runnable animateViewPager;


    public static boolean isAccessedFromDashboard=false;
    public  DashBoardFragment() {
        // Required empty public constructor
    }


    public static DashBoardFragment newInstance(String param1, String param2) {
        DashBoardFragment fragment = new DashBoardFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dash_board, container, false);
        ButterKnife.bind(this,view);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        llHospital.setOnClickListener(this);
        llAmbulance.setOnClickListener(this);
        llHealth_Organization_Homeopathic.setOnClickListener(this);
        llOxygen.setOnClickListener(this);
        llAyurveda_Home.setOnClickListener(this);
        llTherapy_Clinic.setOnClickListener(this);
        llPeoples_Health_Campaign.setOnClickListener(this);
        llNathuropathy_Hospital.setOnClickListener(this);


       /* sliderList = getSliderList();
        loadSliderContent(sliderList);*/

        if(NetworkManager.isInternetConnectionAvailable(getActivity())){
            fetchSliderData();
        }else{
            Snackbar.make(viewPager,getContext().getResources().getString(R.string.no_internet),Snackbar.LENGTH_SHORT).show();
           sliderDTOList = PrefUtils.getSliderList(getContext(),PrefUtils.SLIDER_RESPONSE);
           if(sliderDTOList!=null){
               loadSliderContent(sliderDTOList);
           }
        }
    }


    private void fetchSliderData() {

        final Retrofit retrofit = new RetrofitApiClient(getContext()).getAdapter();
        final APIServices apiServices = retrofit.create(APIServices.class);

        apiServices.getSlider().enqueue(new Callback<SliderResponse>() {

            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                try {

                    if (response.body() != null) {
                        SliderResponse sliderResponse = response.body();

                        sliderDTOList = sliderResponse.getSliderDTOList();
                        PrefUtils.saveSliderList(getContext(),sliderDTOList,PrefUtils.SLIDER_RESPONSE);

                        if(sliderDTOList!=null){
                            loadSliderContent(sliderDTOList);
                        }


                    } else {
                        showSnackbar(getResources().getString(R.string.no_data));
                    }


                } catch (NullPointerException e) {
                    //showProgressDialog(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                Log.d("Failure : ", t.getMessage());
                showSnackbar(getResources().getString(R.string.no_data));
            }
        });


    }

    private void loadSliderContent(List<SliderDTO> sliderItemDTOList) {
        totalSliderCount = sliderItemDTOList.size();
        //  AppLog.showLog(TAG, "sliderItemDTOListSize::::" + sliderItemDTOList.size());
        if (sliderItemDTOList != null && sliderItemDTOList.size() > 0) {

            viewPager.setAdapter(new SlidePagerAdapter(getChildFragmentManager(), sliderItemDTOList));
            //  circlePageIndicator.setViewPager(viewPager);
        }


        startSliderFling();
    }
    private void startSliderFling() {

        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentSliderPage == totalSliderCount) {
                    currentSliderPage = 0;
                }
                viewPager.setCurrentItem(currentSliderPage, true);
                currentSliderPage++;
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);

    }

    void showSnackbar(String message){
        Snackbar.make(llHospital, message, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.llHospital:
                Intent hospitalIntent=new Intent(getActivity(), HospitalActivity.class);
                startActivity(hospitalIntent);
                break;
            case R.id.llAmbulance:
                Intent AmbulanceIntent=new Intent(getActivity(), AmbulanceActivity.class);
                startActivity(AmbulanceIntent);
                break;
            case R.id.llHealth_Organization_Homeopathic:
                Intent homeopathicIntent=new Intent(getActivity(), HomeopathicActivity.class);
                startActivity(homeopathicIntent);

                break;
            case R.id.llOxygen:
                Intent oxygenIntent=new Intent(getActivity(),OxygenActivity.class);
                startActivity(oxygenIntent);
                break;
            case R.id.llTherapy_Clinic:
                Intent therapyIntent=new Intent(getActivity(), TherapyActivity.class);
                startActivity(therapyIntent);

                break;
            case R.id.llAyurveda_Home:
                Intent ayurvedaIntent=new Intent(getActivity(), AyurvedaActivity.class);
                startActivity(ayurvedaIntent);
                break;
            case R.id.llPeoples_Health_Campaign:
                Intent healthintent=new Intent(getActivity(), HealthCampaignActivity.class);
                startActivity(healthintent);

                break;
           case R.id.llNathuropathy_Hospital:
               Intent nathuropathyIntent=new Intent(getActivity(), NathuropathyActivity.class);
               startActivity(nathuropathyIntent);
                break;

        }

    }


    private class SlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<SliderDTO> sliderList;

        public SlidePagerAdapter(FragmentManager fm, List<SliderDTO> mSliderList) {
            super(fm);
            this.sliderList = mSliderList;
            Log.d(TAG, "sliderListSize::" + sliderList.size());
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem::pos:" + position);
            if (sliderList != null) {
                return new BannerFragment(position, sliderList.get(position));
            } else {
                return new BannerFragment(position, null);
            }
        }

        @Override
        public int getCount() {
            return sliderList != null ? sliderList.size() : 1;
        }
    }

}
